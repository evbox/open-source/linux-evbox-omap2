/*
 * Copyright (C) 2019 EVBox B.V. - http://www.evbox.com/
 * Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>
 *
 * SPDX-License-Identifier: GPL2.0-or-later or X11
 *
 */
/dts-v1/;

#include "am33xx.dtsi"

#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/interrupt-controller/irq.h>
#include <dt-bindings/pinctrl/omap.h>
#include <dt-bindings/pwm/pwm.h>

/ {
	model = "EVBox AM335x ComBoard (todo)";
	compatible = "evbox,am335x-comboard-todo", "evbox,am335x-comboard", "ti,am33xx";

	alias {
		serial0 = &uart0;
	};

	cpus {
		cpu@0 {
			cpu0-supply = <&dcdc_1v8_reg>;
		};
	};

	chosen {
		stdout-path = "serial0:115200n8";
		tick-timer = &timer2;
	};

	dcdc_1v1_reg: dcdc_1v1_reg {
		compatible = "regulator-fixed";
		regulator-name = "dcdc_1v1";
		regulator-min-microvolt = <1100000>;
		regulator-max-microvolt = <1100000>;
		regulator-always-on;
		regulator-boot-on;
	};

	dcdc_1v8_reg: dcdc_1v8_reg {
		compatible = "regulator-fixed";
		regulator-name = "dcdc_1v8";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		regulator-always-on;
		regulator-boot-on;
	};

	dcdc_3v3_reg: dcdc_3v3_reg {
		compatible = "regulator-fixed";
		regulator-name = "dcdc_3v3";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		regulator-always-on;
		regulator-boot-on;
	};

	dcdc_3v8_reg: dcdc_3v8_reg {
		pinctrl-names = "default";
		pinctrl-0 = <&dcdc_3v8_pins>;
		compatible = "regulator-fixed";
		regulator-name = "dcdc_3v8";
		regulator-min-microvolt = <3800000>;
		regulator-max-microvolt = <3800000>;
		enable-active-high;
		gpio = <&gpio1 13 GPIO_ACTIVE_HIGH>;
	};

	wlan_vio_3v3_reg: wlan_vio_3v3_reg {
		compatible = "regulator-fixed";
		regulator-name = "wlan_vio_3v3_reg";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		vin-supply = <&dcdc_3v8_reg>;
	};

	leds {
		pinctrl-names = "default";
		pinctrl-0 = <&led_pins>;

		compatible = "gpio-leds";

		led0 {
			label = "comboard:green:dbg0";
			gpios = <&gpio2 9 GPIO_ACTIVE_HIGH>;
			linux,default-trigger = "none";
			default-state = "off";
		};

		led1 {
			label = "comboard:green:dbg1";
			gpios = <&gpio2 10 GPIO_ACTIVE_HIGH>;
			linux,default-trigger = "none";
			default-state = "off";
		};
	};

	memory {
		device_type = "memory";
		reg = <0x80000000 0x8000000>; /* 128 MiB */
	};

	mmc1_pwrseq: mmc1_pwrseq {
		pinctrl-0 = <&mmc1_pwrseq_pins>;
		pinctrl-names = "default";
		compatible = "mmc-pwrseq-simple";
		reset-gpios = <&gpio2 1 GPIO_ACTIVE_LOW>;
	};

	mmc2_pwrseq: mmc2_pwrseq {
		pinctrl-0 = <&emmc_nrst_pins>;
		pinctrl-names = "default";
		compatible = "mmc-pwrseq-emmc";
		reset-gpios = <&gpio0 22 GPIO_ACTIVE_LOW>;
	};

	ncp18xh103 {
		compatible = "murata,ncp18xh103";
		pullup-uv = <1800000>;
		pullup-ohm = <10000>;
		pulldown-ohm = <0>;
		io-channels = <&am335x_adc 0>;
	};

	pwm_32k_clk: pwm_32k_clk {
		compatible = "pwm-clock";
		#clock-cells = <0>;
		clock-output-names = "wlan_32k_clk";
		clock-frequency = <32768>;
		pwms = <&ehrpwm2 0 30518 0>;

		status = "okay";
	};
};

&aes {
	status = "okay";
};

&am335x_adc {
	ti,adc-channels = <0>;
};

&am33xx_pinmux {
	pinctrl-0 = <&clk_32k_pin>;
	pinctrl-names ="default";

	clk_32k_pin: clk_32k_pin {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x9b4, PIN_OUTPUT_PULLDOWN | MUX_MODE3)    /* xdma_event_intr1.clkout2 */
		>;
	};

	cpsw_pins_default: cpsw_pins_default {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x90c, PIN_INPUT_PULLUP | MUX_MODE1)	/* mii1_crs.rmii1_crs_dv */
			AM33XX_IOPAD(0x910, PIN_INPUT_PULLUP | MUX_MODE1)	/* mii1_rxerr.rmii1_rxerr */
			AM33XX_IOPAD(0x914, PIN_OUTPUT_PULLDOWN | MUX_MODE1)	/* mii1_txen.rmii1_txen */
			AM33XX_IOPAD(0x924, PIN_OUTPUT_PULLDOWN | MUX_MODE1)	/* mii1_txd1.mii1_txd1 */
			AM33XX_IOPAD(0x928, PIN_OUTPUT_PULLDOWN | MUX_MODE1)	/* mii1_txd0.mii1_txd0 */
			AM33XX_IOPAD(0x93c, PIN_INPUT_PULLUP | MUX_MODE1)	/* mii1_rxd1.mii1_rxd1 */
			AM33XX_IOPAD(0x940, PIN_INPUT_PULLUP | MUX_MODE1)	/* mii1_rxd0.mii1_rxd0 */
			AM33XX_IOPAD(0x944, PIN_INPUT_PULLUP | MUX_MODE0)	/* rmii1_ref_clk.rmii1_ref_clk */
		>;
	};

	cpsw_pins_sleep: cpsw__pins_sleep {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x90c, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_crs.gpio3_1*/
			AM33XX_IOPAD(0x910, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_rxerr.gpio3_2 */
			AM33XX_IOPAD(0x914, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_txen.gpio3_3 */
			AM33XX_IOPAD(0x924, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_txd1.gpio0_21 */
			AM33XX_IOPAD(0x928, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_txd0.gpio0_28 */
			AM33XX_IOPAD(0x93c, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_rxd1.gpio2_21 */
			AM33XX_IOPAD(0x940, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_rxd0.gpio2_20 */
			AM33XX_IOPAD(0x944, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* rmii1_ref_clk.gpio0_29 */
		>;
	};

	davinci_mdio_pins_default: davinci_mdio_pins_default {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x948, PIN_INPUT_PULLUP | SLEWCTRL_FAST | MUX_MODE0)	/* mdio_data.mdio_data */
			AM33XX_IOPAD(0x94c, PIN_OUTPUT_PULLUP | MUX_MODE0)	/* mdio_clk.mdio_clk */
		>;
	};

	davinci_mdio_pins_sleep: davinci_mdio_pins_sleep {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x948, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mdio_data.gpio0_0*/
			AM33XX_IOPAD(0x94c, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mdio_clk.gpio0_1 */
		>;
	};

	dcdc_3v8_pins: dcdc_3v8_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x834, PIN_OUTPUT | MUX_MODE7)		/* gpmc_ad13.gpio1_13 */
		>;
	};

	ehrpwm2a_pins: ehrpwm2a_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x8a0, PIN_OUTPUT | MUX_MODE3)		/* lcd_data0.ehrpwm2a */
		>;
	};

	emmc_nrst_pins: emmc_nrst_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x820, PIN_OUTPUT_PULLUP | MUX_MODE7)	/* gpmc_ad8.gpio0_22 */
		>;
	};

	led_pins: led_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x8b0, PIN_OUTPUT_PULLDOWN | MUX_MODE7)	/* lcd_data4.gpio2_10 */
			AM33XX_IOPAD(0x8b4, PIN_OUTPUT_PULLDOWN | MUX_MODE7)	/* lcd_data5.gpio2_11 */
		>;
	};

	mmc1_pins: mmc1_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x8f0, PIN_INPUT_PULLUP | MUX_MODE0)	/* mmc0_dat3.mmc0_dat3 */
			AM33XX_IOPAD(0x8f4, PIN_INPUT_PULLUP | MUX_MODE0)	/* mmc0_dat2.mmc0_dat2 */
			AM33XX_IOPAD(0x8f8, PIN_INPUT_PULLUP | MUX_MODE0)	/* mmc0_dat1.mmc0_dat1 */
			AM33XX_IOPAD(0x8fc, PIN_INPUT_PULLUP | MUX_MODE0)	/* mmc0_dat0.mmc0_dat0 */
			AM33XX_IOPAD(0x900, PIN_INPUT_PULLUP | MUX_MODE0)	/* mmc0_clk.mmc0_clk */
			AM33XX_IOPAD(0x904, PIN_INPUT_PULLUP | MUX_MODE0)	/* mmc0_cmd.mmc0_cmd */
		>;
	};

	mmc2_pins: mmc2_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x800, PIN_INPUT_PULLUP | MUX_MODE1)	/* gpmc_ad0.mmc1_dat0 */
			AM33XX_IOPAD(0x804, PIN_INPUT_PULLUP | MUX_MODE1)	/* gpmc_ad1.mmc1_dat1 */
			AM33XX_IOPAD(0x808, PIN_INPUT_PULLUP | MUX_MODE1)	/* gpmc_ad2.mmc1_dat2 */
			AM33XX_IOPAD(0x80c, PIN_INPUT_PULLUP | MUX_MODE1)	/* gpmc_ad3.mmc1_dat3 */
			AM33XX_IOPAD(0x810, PIN_INPUT_PULLUP | MUX_MODE1)	/* gpmc_ad4.mmc1_dat4 */
			AM33XX_IOPAD(0x814, PIN_INPUT_PULLUP | MUX_MODE1)	/* gpmc_ad5.mmc1_dat5 */
			AM33XX_IOPAD(0x818, PIN_INPUT_PULLUP | MUX_MODE1)	/* gpmc_ad6.mmc1_dat6 */
			AM33XX_IOPAD(0x81c, PIN_INPUT_PULLUP | MUX_MODE1)	/* gpmc_ad7.mmc1_dat7 */
			AM33XX_IOPAD(0x880, PIN_INPUT_PULLUP | MUX_MODE2)	/* gpmc_csn1.mmc1_clk */
			AM33XX_IOPAD(0x884, PIN_INPUT_PULLUP | MUX_MODE2)	/* gpmc_csn2.mmc1_cmd */
		>;
	};

	nfc_pins: nfc_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x9b0, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* xdma_event_intr0.gpio0_19*/
		>;
	};

	plc0_pins: plc0_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x824, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* gpmc_ad9.gpio0_23 */
			AM33XX_IOPAD(0x830, PIN_OUTPUT | MUX_MODE7)		/* gpmc_ad12.gpio1_12 */
		>;
	};

//TODO
rtc_control_signals_pins_default: rtc_control_signals_pins_default {
	pinctrl-single,pins = <
		AM33XX_IOPAD(0x9f8, PIN_INPUT | MUX_MODE0) /* (B7) rtc_porz.rtc_porz */
		AM33XX_IOPAD(0x9fc, PIN_INPUT | MUX_MODE0) /* (C7) pmic_power_en.pmic_power_en */
		AM33XX_IOPAD(0xa04, PIN_INPUT | MUX_MODE0) /* (A7) enz_kaldo_1p8v.enz_kaldo_1p8v */
	>;
};

	spi0_pins: spi0_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x950, PIN_INPUT_PULLUP | MUX_MODE0)	/* spi0_sclk.spi0_sclk */
			AM33XX_IOPAD(0x954, PIN_INPUT_PULLUP | MUX_MODE0)	/* spi0_d0.spi0_d0 */
			AM33XX_IOPAD(0x958, PIN_INPUT_PULLUP | MUX_MODE0)	/* spi0_d1.spi0_d1 */
			AM33XX_IOPAD(0x95c, PIN_OUTPUT_PULLUP | MUX_MODE0)	/* spi0_cs0.spi0_cs0 */
			AM33XX_IOPAD(0x960, PIN_OUTPUT_PULLUP | MUX_MODE0)	/* spi0_cs1.spi0_cs1 */
		>;
	};

	spi1_pins: spi1_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x964, PIN_INPUT | MUX_MODE4)		/* ecap0_in_pwm0_out.spi1_sclk */
			AM33XX_IOPAD(0x968, PIN_INPUT_PULLUP | MUX_MODE4)	/* uart0_ctsn.spi1_d0 */
			AM33XX_IOPAD(0x96c, PIN_INPUT_PULLUP | MUX_MODE4)	/* uart0_rtsn.spi1_d1 */
			AM33XX_IOPAD(0x978, PIN_OUTPUT_PULLUP | MUX_MODE4)	/* uart1_ctsn.spi1_cs0 */
		>;
	};

	uart0_pins: uart0_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x970, PIN_INPUT_PULLUP | MUX_MODE0)	/* uart0_rxd.uart0_rxd */
			AM33XX_IOPAD(0x974, PIN_OUTPUT_PULLDOWN | MUX_MODE0)	/* uart0_txd.uart0_txd */
		>;
	};

	uart1_pins: uart1_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x83c, PIN_OUTPUT_PULLDOWN | MUX_MODE7)	/* gpmc_ad15.gpio1_15 */
			AM33XX_IOPAD(0x888, PIN_OUTPUT_PULLDOWN | MUX_MODE7)	/* gpmc_csn3.gpio2_0 */
			AM33XX_IOPAD(0x980, PIN_INPUT_PULLUP | MUX_MODE0)	/* uart1_rxd.uart1_rxd */
			AM33XX_IOPAD(0x984, PIN_OUTPUT_PULLDOWN | MUX_MODE0)	/* uart1_txd.uart1_txd */
		>;
	};

	uart3_pins: uart3_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x8c8, PIN_INPUT_PULLUP | MUX_MODE6)	/* lcd_data10.uart3_ctsn */
			AM33XX_IOPAD(0x8cc, PIN_OUTPUT_PULLUP | MUX_MODE6)	/* lcd_data11.uart3_rtsn */
			AM33XX_IOPAD(0x934, PIN_INPUT_PULLUP | MUX_MODE1)	/* mii1_rxd3.uart3_rxd */
			AM33XX_IOPAD(0x938, PIN_OUTPUT_PULLDOWN | MUX_MODE1)	/* mii1_rxd2.uart3_txd */
		>;
	};

	wake_pins: wake_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x920, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* mii1_txd2.gpio0_17*/
		>;
	};

	mmc1_pwrseq_pins: mmc1_pwrseq_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0x88c, PIN_OUTPUT | MUX_MODE7)		/* gpmc_clk.gpio2_1 */
		>;
	};

	wlan0_pins: wlan0_pins {
		pinctrl-single,pins = <
			AM33XX_IOPAD(0xa1c, PIN_INPUT_PULLDOWN | MUX_MODE7)	/* gpmc_ad10.gpio0_18 */
//			wakeup,
//			AM33XX_IOPAD(0x908, PIN_OUTPUT_PULLDOWN | MUX_MODE7)	/* mii1_col.gpio3_0 */
		>;
	};
};

&cppi41dma  {
	status = "okay";
};

&cpsw_emac0 {
	phy-handle = <&ethernet_phy0>;
	phy-mode = "rmii";

	status = "okay";
};

&davinci_mdio {
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&davinci_mdio_pins_default>;
	pinctrl-1 = <&davinci_mdio_pins_sleep>;

	status = "okay";

	ethernet_phy0: ethernet_phy@1 {
		reg = <1>;
	};
};

&ehrpwm2 {
	pinctrl-names = "default";
	pinctrl-0 = <&ehrpwm2a_pins>;

	status = "okay";
};

&epwmss2 {
	status = "okay";
};

&mac {
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&cpsw_pins_default>;
	pinctrl-1 = <&cpsw_pins_sleep>;

	status = "okay";
};

&mmc1 {
	pinctrl-names = "default";
	pinctrl-0 = <&mmc1_pins>;
	bus-width = <0x4>;
	ti,needs-special-hs-handling;
	non-removable;
	no-1-8-v;
	mmc-pwrseq = <&mmc1_pwrseq>;
	cap-sdio-irq;
	vmmc-supply = <&wlan_vio_3v3_reg>;

	status = "okay";

	wlan0@1 {
//		pinctrl-0 = <&wlan0_pins>;
//		pinctrl-names = "default";
		reg = <1>;
		compatible = "brcm,bcm4329-fmac";
		clocks = <&pwm_32k_clk>;
		clock-names = "lpo_in_clk";
//		interrupt-parent = <&gpio0>;
//		interrupts = <18 IRQ_TYPE_EDGE_RISING>;
//		interrupt-names = "host-wake";
		//wake-gpios = <&gpio2 1 GPIO_ACTIVE_HIGH>;
	};
};

&mmc2 {
	pinctrl-names = "default";
	pinctrl-0 = <&mmc2_pins>;
	bus-width = <0x8>;
	ti,needs-special-hs-handling;
	non-removable;
	no-1-8-v;
	cap-mmc-hw-reset;
	mmc-pwrseq = <&mmc2_pwrseq>;
	vmmc-supply = <&dcdc_3v3_reg>;

	status = "okay";

	emmc@0 {
		reg = <0>;
		compatible = "mmc-card";
	};
};

&phy_sel {
	rmii-clock-ext;
};

&rtc {
//	clocks = <&clk_24mhz_clkctrl AM3_CLK_24MHZ_CLKDIV32K_CLKCTRL 0>;
//	clock-names = "int-clk";
	system-power-controller;

	status = "okay";
};

&sham {
	status = "okay";
};

&spi0 {
	pinctrl-names = "default";
	pinctrl-0 = <&spi0_pins>;

	status = "okay";

	flash@0 {
		reg = <0>;
		compatible = "issi,is25lq010b", "jedec,spi-nor";
		spi-max-frequency = <12000000>;
		m25p,fast-read;

		partitions {
			compatible = "fixed-partitions";
			#address-cells = <1>;
			#size-cells = <1>;

			partition@0 {
				label = "u-boot-spl";
				reg = <0x00000 0x1c000>;
			};

			partition@1c000 {
				label = "sys_conf";
				reg = <0x1c000 0x03000>;
				read-only;
			};
		};
	};

	spidev@1 {
		reg = <1>;
		pinctrl-names = "default";
		pinctrl-0 = <&nfc_pins>;
		compatible = "spidev";
		spi-max-frequency = <500000000>;
	};
/*
	nfc@1 {
		reg = <1>;
		pinctrl-names = "default";
		pinctrl-0 = <&nfc_pins>;
		compatible = "st,st25r3912";
		spi-max-frequency = <500000000>;
		interrupt-parent = <&gpio0>;
		//interrupts = <19 IRQ_TYPE_LEVEL_LOW>;
		//interrupt-names = "host-wake";
	};
*/
};

&spi1 {
	pinctrl-names = "default";
	pinctrl-0 = <&spi1_pins>;
	ti,pindir-d0-out-d1-in;

	status = "okay";

	plc@0 {
		reg = <0>;
		compatible = "qca,qca7000";
		pinctrl-names = "default";
		pinctrl-0 = <&plc0_pins>;
		interrupt-parent = <&gpio0>;
		interrupts = <23 IRQ_TYPE_EDGE_RISING>;
		spi-cpha;
		spi-cpol;
		spi-max-frequency = <12000000>;
		reset-gpios = <&gpio1 12 GPIO_ACTIVE_LOW>;
	};
};

&tscadc {
	status = "okay";
};

&uart0 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart0_pins>;

	status = "okay";
};

&uart1 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart1_pins>;
	linux,rs485-enabled-at-boot-time;
	rts-gpio = <&gpio1 15 GPIO_ACTIVE_HIGH>;
	dtr-gpio = <&gpio2 0 GPIO_ACTIVE_LOW>;
	rs485-rts-active-high;
// TODO is this really needed? why is 1 enough?
	rs485-rts-delay = <1 1>;

	status = "okay";
};

&uart3 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart3_pins>;
	uart-has-rtscts;

	status = "okay";
};

&usb {
	status = "okay";
};

&usb_ctrl_mod {
	status = "okay";
};

&usb0_phy {
	status = "okay";
	vcc-supply = <&dcdc_3v8_reg>;
	pinctrl-names = "default";
};

&usb0 {
	status = "okay";
	dr_mode = "host";
	#address-cells = <1>;
	#size-cells = <0>;
};
