# EVBox Kernel
This repository contains build scripts to build the EVBox kernel used to bring
up the Race Com Board.

Currently this repository wraps around the [Linux](https://www.kernel.org/) kernel.

The build script can be run using the *-h* parameter for more information about
arguments and environment variables.

## Building
After cloning this repository using **`git clone --recursive`** (when forgetting
the --recursive parameter to git clone, use `git submodule update --init --recursive`
afterwards), there are two methods to build the kernel. Either a local build,
which requires all build requirements are resolved locally, or using a
[Docker](https://www.docker.org) container.

### Docker build
As a convenience a script called `esbs/docker_build.sh` is linked to in this
repository. Building is as simple as running the convenience script with any
parameters. So to yield the kernels help, making use of the local build.sh symlink:
```console
./build.sh help
```
To view the wrappers options, supply it with the *-h* parameter.
```console
./build.sh -h
```

### Local build
While the docker build is strongly recommended, calling the `esbs/build.sh`
script will run 'make' in the linux sub directory passing along any arguments
supplied. In other words, to run **_make menuconfig_** use the script as
`./esbs/build.sh menuconfig`.

Note that all build dependencies need to be manually resolved on the host.
